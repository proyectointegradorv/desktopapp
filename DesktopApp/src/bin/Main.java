package bin;

import controller.User;
import view.MainFrame;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static final MainFrame frame = new MainFrame();
    public static final User user = new User();


    public static void YesNoDialog(MainFrame frame){
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showConfirmDialog(null, "¿Desea salir del programa?",
                        "Cerrar Programa", JOptionPane.YES_NO_OPTION);
                if (confirm == JOptionPane.YES_OPTION) {
                    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                } else {
                    frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                }
            }
        });
    }

    public static void visibleFrame(boolean visibility){
        frame.setVisible(visibility);
    }

    public static void main(String[] args) throws NullPointerException {
        try {

            //Frame Sets

        frame.setTitle("Práctica");
        frame.pack();
        frame.setSize(1100, 700);
        visibleFrame(true);
        frame.setLocationRelativeTo(null);

        user.credentials();

        YesNoDialog(frame);


    } catch (NullPointerException | NoSuchAlgorithmException e){
            System.err.println("The variable is not instantiated: "+e);
        }
  }

}
