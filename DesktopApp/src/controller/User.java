package controller;





import bin.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.CreateUserAccountFrame;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;


public class User {

    protected static final Logger logger = LoggerFactory.getLogger(User.class);
    protected static final JTextField userField = new JTextField("User",10);
    protected static final JPasswordField passwdField = new JPasswordField( "Password", 10);

    public void credentials() throws NoSuchAlgorithmException {

        //Beautify Design
        try{
            UIManager.setLookAndFeel("com.formdev.flatlaf.intellijthemes.FlatCobalt2IJTheme");
        } catch(Exception e){
            System.out.println("Look and Feel error -> "+e);
        }

        JPanel myPanel = new JPanel();
        JLabel userL = new JLabel("Usuario   ");
        JLabel passwdL = new JLabel("Contraseña   ");
        JLabel star = new JLabel("*");
        JLabel star1 = new JLabel("*");
        JLabel hyperlink = new JLabel("Conócenos en nuestro sitio web");

        userL.setForeground(Color.WHITE);
        passwdL.setForeground(Color.WHITE);
        hyperlink.setForeground(Color.YELLOW);
        star.setForeground(Color.RED);
        star1.setForeground(Color.RED);

        hyperlink.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        myPanel.add(userL);
        myPanel.add(star1);
        myPanel.add(userField);
        myPanel.add(Box.createHorizontalStrut(20)); // a spacer
        myPanel.setBackground(new Color(26, 28, 65));
        myPanel.add(passwdL);
        myPanel.add(star);
        myPanel.add(passwdField);
        myPanel.add(Box.createHorizontalStrut(20)); // a spacer
        myPanel.add(hyperlink);

        hyperlink.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                    try {
                        Desktop.getDesktop().browse(new URI("http://localhost/ProyectoIntegradorV/practica/index.html"));

                    } catch (IOException | URISyntaxException ex) {
                        ex.printStackTrace();
                    }
            }
        });

        userField.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                if(userField.getText().trim().equalsIgnoreCase("user")){
                    userField.setText("");
                    userField.setForeground(Color.WHITE);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(userField.getText().trim().equals("") || passwdField.getText().trim().equalsIgnoreCase("user")){
                    userField.setText("User");
                    userField.setForeground(Color.ORANGE);
                }
            }
        });

        passwdField.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent e){
                if(passwdField.getText().trim().equalsIgnoreCase("password")){
                    passwdField.setText("");
                    passwdField.setForeground(Color.WHITE);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(passwdField.getText().trim().equals("") || passwdField.getText().trim().equalsIgnoreCase("password")){
                    passwdField.setText("Password");
                    passwdField.setForeground(Color.ORANGE);
                }
            }
        });

       User user = new User();
       user.login(myPanel, userField);

    }

    public int login(JPanel myPanel, JTextField userField) {

        Object[] options = { "Iniciar Sesión", "Registrar/Actualizar", "Salir"};

        int result = JOptionPane.showOptionDialog(null, myPanel, "Credenciales de Usuario",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, null);

        if (result == JOptionPane.YES_OPTION) {
            if (getUserField().equals(CreateUserAccountFrame.getAliasUAF()) && Strategy.Password(getPasswdField()).equals(CreateUserAccountFrame.getPasswordUAF())) {
                logger.info("¡Bienvenido usuario "+CreateUserAccountFrame.getAliasUAF()+"!");
                JOptionPane.showMessageDialog(null, "¡Bienvenido a Práctica, usuario " + userField.getText() + "!", "Autenticación exitosa", JOptionPane.INFORMATION_MESSAGE);
                return JOptionPane.YES_OPTION;
            } else {
                logger.info("Error de credenciales");
                JOptionPane.showMessageDialog(null, "¡Error de credenciales, vuelva a intentarlo!", "Usuario no existente", JOptionPane.ERROR_MESSAGE);
                return login(myPanel, userField);
            }
        } else {
            if(result == JOptionPane.NO_OPTION){
                String[] org = new String[1];
                CreateUserAccountFrame.main(org);
                Main.visibleFrame(false);
                return JOptionPane.NO_OPTION;
            } else {
                System.exit(0);
            }
        }
        return 0;
    }

    public String getUserField() {
        return userField.getText();
    }

    public static String getPasswdField() {
        return passwdField.getText();
    }
}


