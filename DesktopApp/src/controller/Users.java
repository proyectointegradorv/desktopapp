package controller;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Users{
    private String cod;
    private String uname;
    private String password;
    private Boolean sqlExec;

    public Users(String cod, String uname, String password){
        this.cod = cod;
        this.uname = uname;
        this.password = password;
    }
    
    public Users(String uname, String password){
        this.cod = "";
        this.uname = uname;
        this.password = password;
    }


    public Users(String cod){
        this.cod = cod;
        this.uname = "";
        this.password = "";
    }

    public Users(){
        this.cod = "";
        this.uname = "";
        this.password = "";
    }

    public boolean insert()throws InstantiationException, ClassNotFoundException,IllegalAccessException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = ((ConnectionDB) connection).registrarUsuario(uname, password);
        return sqlExec;
    }

    public boolean delete()throws InstantiationException,
        ClassNotFoundException,IllegalAccessException, SQLException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.eliminarUsuario(uname, password);
        return sqlExec;
    }

    public boolean update()throws InstantiationException, ClassNotFoundException,
           IllegalAccessException, SQLException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = ((ConnectionDB) connection).modificarUsuario(cod, uname, password);
        return sqlExec;
    }

    public String read()throws InstantiationException, ClassNotFoundException,
           IllegalAccessException, SQLException{
	sqlExec = false;
	ResultSet result;
	String resultQuery = "";
	ConnectionDB connection = new ConnectionDB();
	result = connection.consultarUsuario();
	while (result.next()){
	    resultQuery = resultQuery + result.getString (1) + "\n" + result.getString (2)+ "\n" + result.getString(3) + "\n";
	}
    	return resultQuery;
    }
}
