package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;
//import com.mysql.jdbc.Connection;

public class ConnectionDB {

    private Connection connect = null;
    private ResultSet result;

    public ConnectionDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            System.out.println("Conexión realizada con éxito.");
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException e) {
                System.out.println("Error -> "+e);
            }
        }
    }

        public Connection host()
        {
            try {
                //Cargamos el Driver MySQL
                Class.forName("com.mysql.jdbc.Driver");

                connect = DriverManager.getConnection("jdbc:mysql://localhost/practicaDB","root","");
                System.out.println("conexion establecida");
               // JOptionPane.showMessageDialog(null,"Conectado");
               
            } catch (ClassNotFoundException | SQLException e) {
                System.out.println("error de conexion");
                //JOptionPane.showMessageDialog(null,"Error de conexion"+e);
            }
            return connect;
        }

    /*---------------------------------------------------------------------------*/
    /*                              Estudiante                                   */
    /*---------------------------------------------------------------------------*/

    //Registrar estudiante
    public boolean registrarEstudiante(String doc, String tipoDoc, String priNom, String segNom,
                                       String terNom, String priApe, String segApe, String sex,
                                       String fecNac, String lugNac, String car, String sem,
                                       String dir, String bar, String tel, String email) throws
            InstantiationException, ClassNotFoundException, IllegalAccessException {
        String insert = "INSERT INTO Est (cod, doc, tipoDoc, priNom, segNom, terNom, priApe, secApe," +
                "sexo, fecNac, lugNac,  carrera, semestre, dirResi, barrioResi, tel, email)" +
                "VALUES (NULL, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB",
                    "root", "");
            PreparedStatement command = connect.prepareStatement(insert);
            command.setString(1, doc);
            command.setString(2, tipoDoc);
            command.setString(3, priNom);
            command.setString(4, segNom);
            command.setString(5, terNom);
            command.setString(6, priApe);
            command.setString(7, segApe);
            command.setString(8, sex);
            command.setString(9, fecNac);
            command.setString(10, lugNac);
            command.setString(11, car);
            command.setString(12, sem);
            command.setString(13, dir);
            command.setString(14, bar);
            command.setString(15, tel);
            command.setString(16, email);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro exitoso");
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> \n" + e);
            return false;
        }
    }

    //Eliminar estudiante
    public boolean eliminarEstudiante(String doc) throws
            SQLException {
        String delete = "DELETE FROM Est WHERE doc = ?";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            PreparedStatement command = connect.prepareStatement(delete);
            command.setString(1, doc);
            command.execute();
            JOptionPane.showMessageDialog(null, "Eliminación exitosa");
            return true;
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> "+e);
            return false;
        }
    }

    //Consultar estudiante
    public ResultSet consultarEstudiante() throws SQLException {
        String read = "SELECT * FROM Est";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            PreparedStatement command = connect.prepareStatement(read);
            result = command.executeQuery();
            while(result.next()){
                
            }
            return result;
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> "+e);
            return null;
        }
    }

    //Modificar estudiante
    public boolean modificarEstudiante(String doc, String tipoDoc, String priNom, String segNom,
                                       String terNom, String priApe, String segApe, String sex,
                                       String fecNac, String lugNac, String car, String sem,
                                       String dir, String bar, String tel, String email) throws
            SQLException {
        String update = "UPDATE Est SET " +
                "tipoDoc = ?, " +
                "priNom = ?, " +
                "segNom = ?, " +
                "terNom = ?, " +
                "priApe = ?, " +
                "secApe = ?, " +
                "sexo = ?, " +
                "fecNac = ?, " +
                "lugNac = ?, " +
                "carrera = ?, " +
                "semestre = ?, " +
                "dirResi = ?, " +
                "barrioResi = ?, " +
                "tel = ?, " +
                "email = ? " +
                "WHERE doc = ?";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            PreparedStatement command = connect.prepareStatement(update);
            command.setString(1, tipoDoc);
            command.setString(2, priNom);
            command.setString(3, segNom);
            command.setString(4, terNom);
            command.setString(5, priApe);
            command.setString(6, segApe);
            command.setString(7, sex);
            command.setString(8, fecNac);
            command.setString(9, lugNac);
            command.setString(10, car);
            command.setString(11, sem);
            command.setString(12, dir);
            command.setString(13, bar);
            command.setString(14, tel);
            command.setString(15, email);
            command.setString(16, doc);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Estudiante modificado correctamente");
            return true;
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error code: 409 -> "+e);
            return false;
        }
    }


    /*---------------------------------------------------------------------------*/
    /*                              Usuario                                      */
    /*---------------------------------------------------------------------------*/

    //Registrar usuario
    public boolean registrarUsuario(String uname, String password) throws
            InstantiationException, ClassNotFoundException, IllegalAccessException {
        String insert = "INSERT INTO Users (cod, uname, password)" +
                "VALUES (NULL, ?, ?)";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB",
                    "root", "");
            PreparedStatement command = connect.prepareStatement(insert);
            command.setString(1, uname);
            command.setString(2, password);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro exitoso");
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> \n" + e);
            return false;
        }
    }

    //Eliminar usuario
    public boolean eliminarUsuario(String uname, String password) throws
            SQLException {
        String delete = "DELETE FROM Users WHERE uname = ? && password = ?";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            PreparedStatement command = connect.prepareStatement(delete);
            command.setString(1, uname);
            command.setString(2, password);
            command.execute();
            JOptionPane.showMessageDialog(null, "Eliminación exitosa");
            return true;
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> "+e);
            return false;
        }
    }

    //Consultar usuario
    public ResultSet consultarUsuario() throws SQLException {
        String read = "SELECT * FROM Users";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            PreparedStatement command = connect.prepareStatement(read);
            result = command.executeQuery();
            while(result.next()){

            }
            return result;
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> "+e);
            return null;
        }
    }

    //Modificar usuario
    public boolean modificarUsuario(String cod, String uname, String password) throws
            SQLException {
        String update = "UPDATE Users SET " +
                "uname = ?, " +
                "password = ? " +
                "WHERE cod = ?";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");
            PreparedStatement command = connect.prepareStatement(update);
            command.setString(1, uname);
            command.setString(2, password);
            command.setString(3, cod);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Usuario modificado correctamente");
            return true;
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error code: 409 -> "+e);
            return false;
        }
    }

    /*°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°*/
    /*°                                                       °*/
    /*°                       Company                         °*/
    /*°                                                       °*/
    /*°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°*/

    //Create Company

    public boolean createCompany(int NIT_Company, String Name_Company, String Residence_Town, String Residence_State,
                                 String Residence_Country, String Type_Company, String Residence_Address, String Type_Contact,
                                 int Number_Contact, String Website_Contact, String Status_Company, String Origin_Company,
                                 String Number_Employees) {
        String INSERT = "INSERT INTO tbl_empresa (NIT, nombre_empresa, ciudad_sede, departamento_sede, pais_sede, " +
                "tipo_empresa, direccion_sede, tipo_contacto, numero_contacto, website_contacto, estado_empresa, " +
                "origen_empresa, numero_empleados) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(INSERT);

            command.setInt(1, NIT_Company);
            command.setString(2, Name_Company);
            command.setString(3, Residence_Town);
            command.setString(4, Residence_State);
            command.setString(5, Residence_Country);
            command.setString(6, Type_Company);
            command.setString(7, Residence_Address);
            command.setString(8, Type_Contact);
            command.setInt(9, Number_Contact);
            command.setString(10, Website_Contact);
            command.setString(11, Status_Company);
            command.setString(12, Origin_Company);
            command.setString(13, Number_Employees);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro exitoso");
            return true;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return false;
        }

    }

    //Modify Company

    public boolean modifyCompany(int NIT_Company, String Name_Company, String Residence_Town, String Residence_State,
                                 String Residence_Country, String Type_Company, String Residence_Address, String Type_Contact,
                                 int Number_Contact, String Website_Contact, String Status_Company, String Origin_Company,
                                 String Number_Employees) {
        String UPDATE = "UPDATE tbl_empresa SET nombre_empresa = ?, ciudad_sede = ?, departamento_sede = ?, pais_sede = ?, " +
                "tipo_empresa = ?, direccion_sede = ?, tipo_contacto = ?, numero_contacto = ?, website_contacto = ?, estado_empresa = ?, " +
                "origen_empresa = ?, numero_empleados = ? WHERE NIT = ?";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(UPDATE);

            command.setString(1, Name_Company);
            command.setString(2, Residence_Town);
            command.setString(3, Residence_State);
            command.setString(4, Residence_Country);
            command.setString(5, Type_Company);
            command.setString(6, Residence_Address);
            command.setString(7, Type_Contact);
            command.setInt(8, Number_Contact);
            command.setString(9, Website_Contact);
            command.setString(10, Status_Company);
            command.setString(11, Origin_Company);
            command.setString(12, Number_Employees);
            command.setInt(13, NIT_Company);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Modificación exitosa");
            return true;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return false;
        }

    }

    //Delete Company

    public boolean deleteCompany ( int NIT_Company) {
        String DELETE = "DELETE FROM tbl_empresa WHERE NIT = ?";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(DELETE);

            command.setInt(1, NIT_Company);
            command.execute();
            JOptionPane.showMessageDialog(null, "Eliminación exitosa");
            return true;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return false;
        }

    }

    //Query Company

    public ResultSet queryCompany ( int NIT_Company) {
        String QUERY = "SELECT * FROM tbl_empresa WHERE NIT = ?";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(QUERY);

            command.setInt(1, NIT_Company);
            result = command.executeQuery();
            JOptionPane.showMessageDialog(null, "Retorno de datos exitoso");
            return result;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return null;
        }

    }

    /*°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°*/
    /*°                                                       °*/
    /*°                       UserAccount                     °*/
    /*°                                                       °*/
    /*°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°*/

    //Create User Account

    public boolean createUser(String Name_userAccount, String Alias_userAccount, String LName_userAccount, String Email_userAccount,
                                 String Password_userAccount) {
        String INSERT = "INSERT INTO tbl_user_account (nombre_usuario, alias_usuario, apellidos_usuario, email_usuario, " +
                "contrasenia_usuario) VALUES (?, ?, ?, ?, ?)";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(INSERT);

            command.setString(1, Name_userAccount);
            command.setString(2, Alias_userAccount);
            command.setString(3, LName_userAccount);
            command.setString(4, Email_userAccount);
            command.setString(5, Password_userAccount);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro exitoso");
            return true;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return false;
        }

    }

    //Modify User Account

    public boolean modifyUser(String Name_userAccount, String Alias_userAccount, String LName_userAccount, String Email_userAccount,
                              String Password_userAccount) {
        String UPDATE = "UPDATE tbl_user_account SET nombre_usuario = ?, apellidos_usuario = ?, " +
                "email_usuario = ?, contrasenia_usuario = ? WHERE alias_usuario = ?";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(UPDATE);

            command.setString(1, Name_userAccount);
            command.setString(2, LName_userAccount);
            command.setString(3, Email_userAccount);
            command.setString(4, Password_userAccount);
            command.setString(5, Alias_userAccount);
            command.executeUpdate();
            JOptionPane.showMessageDialog(null, "Modificación exitosa");
            return true;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return false;
        }

    }

    // Delete User Account

    public boolean deleteUser ( String Alias_userAccount) {
        String DELETE = "DELETE FROM tbl_user_account WHERE alias_usuario = ?";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/practicaDB", "root", "");

            PreparedStatement command = connect.prepareStatement(DELETE);

            command.setString(1, Alias_userAccount);
            command.execute();
            JOptionPane.showMessageDialog(null, "Eliminación exitosa");
            return true;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error code: 409 -> " + ex);
            return false;
        }

    }

}
