package controller;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Estudiante{
    private String doc;
    private String tipoDoc;
    private String priNom;
    private String segNom;
    private String terNom;
    private String priApe;
    private String segApe;
    private String sex;
    private String fecNac;
    private String lugNac;
    private String car;
    private String sem;
    private String dir;
    private String bar;
    private String tel;
    private String email;
    private Boolean sqlExec;

    public Estudiante(String doc, String tipoDoc, String priNom, String segNom,
                                String terNom, String priApe, String segApe, String sex,
                                String fecNac, String lugNac, String car, String sem,
                                String dir, String bar, String tel, String email){
        this.doc = doc;
        this.tipoDoc = tipoDoc;
        this.priNom = priNom;
        this.segNom = segNom;
        this.terNom = terNom;
        this.priApe = priApe;
        this.segApe = segApe;
        this.sex = sex;
        this.fecNac = fecNac;
        this.lugNac = lugNac;
        this.car = car;
        this.sem = sem;
        this.dir = dir;
        this.bar = bar;
        this.tel = tel;
        this.email = email;
    }

    public Estudiante(String doc){
        this.doc = doc;
        this.tipoDoc = "";
        this.priNom = "";
        this.segNom = "";
        this.terNom = "";
        this.priApe = "";
        this.segApe = "";
        this.sex = "";
        this.fecNac = "";
        this.lugNac = "";
        this.car = "";
        this.sem = "";
        this.dir = "";
        this.bar = "";
        this.tel = "";
        this.email = "";
    }

    public Estudiante(){
        this.doc = "";
        this.tipoDoc = "";
        this.priNom = "";
        this.segNom = "";
        this.terNom = "";
        this.priApe = "";
        this.segApe = "";
        this.sex = "";
        this.fecNac = "";
        this.lugNac = "";
        this.car = "";
        this.sem = "";
        this.dir = "";
        this.bar = "";
        this.tel = "";
        this.email = "";
    }    

    public boolean insert()throws InstantiationException, ClassNotFoundException,IllegalAccessException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = ((ConnectionDB) connection).registrarEstudiante(doc, tipoDoc, priNom, segNom, terNom,
                                                                                                                priApe, segApe, sex, fecNac, lugNac, car,
                                                                                                                sem, dir, bar, tel, email);
        return sqlExec;
    }

    public boolean delete()throws InstantiationException,
        ClassNotFoundException,IllegalAccessException, SQLException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.eliminarEstudiante(doc);
        return sqlExec;
    }

    public boolean update()throws InstantiationException, ClassNotFoundException,
           IllegalAccessException, SQLException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = ((ConnectionDB) connection).modificarEstudiante(doc, tipoDoc, priNom, segNom, terNom,
                                                                                                                priApe, segApe, sex, fecNac, lugNac, car,
                                                                                                                sem, dir, bar, tel, email);
        return sqlExec;
    }

    public String read()throws InstantiationException, ClassNotFoundException,
           IllegalAccessException, SQLException{
	sqlExec = false;
	ResultSet result;
	String resultQuery = "";
	ConnectionDB connection = new ConnectionDB();
	result = connection.consultarEstudiante();
	while (result.next()){
	    resultQuery = resultQuery + result.getString (1) + "\n" + result.getString (2)+ "\n" + 
                      result.getString(3) + "\n" + result.getString (4) + "\n" + result.getString(5) + "\n" + 
                      result.getString(6) + "\n" + result.getString (7) + "\n" + result.getString(8) + "\n" +
                      result.getString (9) + "\n" + result.getString (10)+ "\n" + 
                      result.getString(11) + "\n" + result.getString (12) + "\n" + result.getString(13) + "\n" + 
                      result.getString(14)+ "\n" + result.getString (15) + "\n" + result.getString(16) + "\n";
	}
    	return resultQuery;
    }

    public String getDoc() {
        return doc;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public String getPriNom() {
        return priNom;
    }

    public String getSegNom() {
        return segNom;
    }

    public String getTerNom() {
        return terNom;
    }

    public String getPriApe() {
        return priApe;
    }

    public String getSegApe() {
        return segApe;
    }

    public String getSex() {
        return sex;
    }

    public String getFecNac() {
        return fecNac;
    }

    public String getLugNac() {
        return lugNac;
    }

    public String getCar() {
        return car;
    }

    public String getSem() {
        return sem;
    }

    public String getDir() {
        return dir;
    }

    public String getBar() {
        return bar;
    }

    public String getTel() {
        return tel;
    }

    public String getEmail() {
        return email;
    }
}
