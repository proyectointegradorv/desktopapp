package controller;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class Strategy {

    public static String Password(String data) {
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(data.getBytes(StandardCharsets.UTF_8));
            byte[] digestBytes = messageDigest.digest();

            String hex;
            for (byte digestByte : digestBytes) {
                hex = Integer.toHexString(0xFF & digestByte);
                if (hex.length() < 2)
                    sb.append("0");
                sb.append(hex);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return new String(sb);
    }
}

