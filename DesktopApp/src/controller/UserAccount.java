package controller;

import java.sql.SQLException;

public class UserAccount {

    protected String Name_userAccount;
    protected String Alias_userAccount;
    protected String LName_userAccount;
    protected String Email_userAccount;
    protected String Password_userAccount;
    protected Boolean sqlExec;

    public UserAccount(String name, String alias, String lname, String email, String passwd){
        this.Name_userAccount = name;
        this.Alias_userAccount = alias;
        this.LName_userAccount = lname;
        this.Email_userAccount = email;
        this.Password_userAccount = passwd;
    }

    public UserAccount(String alias){
        this.Name_userAccount = "";
        this.Alias_userAccount = alias;
        this.LName_userAccount = "";
        this.Email_userAccount = "";
        this.Password_userAccount = "";
    }

    public UserAccount(String alias, String passwd){
        this.Name_userAccount = "";
        this.Alias_userAccount = alias;
        this.LName_userAccount = "";
        this.Email_userAccount = "";
        this.Password_userAccount = passwd;
    }

    public boolean createUserAccount() throws InstantiationException, ClassNotFoundException, IllegalAccessException, SQLException {
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.createUser(Name_userAccount, Alias_userAccount, LName_userAccount, Email_userAccount,
                Password_userAccount);
        return sqlExec;
    }

    public boolean updateUserAccount()throws InstantiationException, ClassNotFoundException,
            IllegalAccessException, SQLException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.modifyUser(Name_userAccount, Alias_userAccount, LName_userAccount, Email_userAccount,
                Password_userAccount);
        return sqlExec;
    }

    public boolean deleteUserAccount()throws InstantiationException,
            ClassNotFoundException,IllegalAccessException, SQLException {
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.deleteUser(Alias_userAccount);
        return sqlExec;
    }

    public String getAlias_userAccount() {
        return Alias_userAccount;
    }

    public String getPassword_userAccount() {
        return Password_userAccount;
    }
}
