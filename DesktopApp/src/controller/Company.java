package controller;


import java.sql.*;


public class Company {

    protected int NIT_Company;
    protected String Name_Company;
    protected String Residence_Town;
    protected String Residence_State;
    protected String Residence_Country;
    protected String Type_Company;
    protected String Residence_Address;
    protected String Type_Contact;
    protected int Number_Contact;
    protected String Website_Contact;
    protected String Status_Company;
    protected String Origin_Company;
    protected String Number_Employees;
    protected Boolean sqlExec;

    public Company(int NIT_Company, String Name_Company, String Residence_Town, String Residence_State,
                   String Residence_Country, String Type_Company, String Residence_Address, String Type_Contact,
                   int Number_Contact, String Website_Contact, String Status_Company, String Origin_Company,
                   String Number_Employees){
        this.NIT_Company = NIT_Company;
        this.Name_Company = Name_Company;
        this.Residence_Town = Residence_Town;
        this.Residence_State = Residence_State;
        this.Residence_Country = Residence_Country;
        this.Type_Company = Type_Company;
        this.Residence_Address = Residence_Address;
        this.Type_Contact = Type_Contact;
        this.Number_Contact = Number_Contact;
        this.Website_Contact = Website_Contact;
        this.Status_Company = Status_Company;
        this.Origin_Company = Origin_Company;
        this.Number_Employees = Number_Employees;
    }

    public Company(int NIT_Company){
        this.NIT_Company = NIT_Company;
        this.Name_Company = "";
        this.Residence_Town = "";
        this.Residence_State = "";
        this.Residence_Country = "";
        this.Type_Company = "";
        this.Residence_Address = "";
        this.Type_Contact = "";
        this.Number_Contact = 0;
        this.Website_Contact = "";
        this.Status_Company = "";
        this.Origin_Company = "";
        this.Number_Employees = "";
    }

    public boolean create() throws InstantiationException, ClassNotFoundException, IllegalAccessException, SQLException {
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.createCompany(NIT_Company, Name_Company, Residence_Town, Residence_State,
                Residence_Country, Type_Company, Residence_Address, Type_Contact,
        Number_Contact, Website_Contact, Status_Company, Origin_Company, Number_Employees);
        return sqlExec;
    }

    public boolean delete()throws InstantiationException,
            ClassNotFoundException,IllegalAccessException, SQLException {
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.deleteCompany(NIT_Company);
        return sqlExec;
    }

    public boolean update()throws InstantiationException, ClassNotFoundException,
            IllegalAccessException, SQLException{
        sqlExec = false;
        ConnectionDB connection = new ConnectionDB();
        sqlExec = connection.modifyCompany(NIT_Company, Name_Company, Residence_Town, Residence_State,
                Residence_Country, Type_Company, Residence_Address, Type_Contact,
                Number_Contact, Website_Contact, Status_Company, Origin_Company, Number_Employees);
        return sqlExec;
    }

   /* public String read()throws InstantiationException, ClassNotFoundException,
            IllegalAccessException, SQLException{
        sqlExec = false;
        ResultSet result;
        String resultQuery = "";
        ConnectionDB connection = new ConnectionDB();
        result = connection.queryCompany(NIT_Company);
        while (result.next()){
            resultQuery = resultQuery +"Información de la Empresa: \n\t\t NIT: "+ result.getInt (2) + "\n\t\t Nombre de la Empresa: "+ result.getString (3)+ "\n\t\t Ciudad sede: " +
                    result.getString(4) + "\n\t\t Departamento sede: "+ result.getString (5) + "\n\t\t País sede: "+ result.getString(6) + "\n\t\t Tipo de Empresa: "+
                    result.getString(7) + "\n\t\t Dirección sede: "+ result.getString (8) + "\n\t\t Tipo contacto: "+ result.getString(9) + "\n\t\t Número contacto: "+
                    result.getInt (10) + "\n\t\t Sitio Web contacto: "+ result.getString (11)+ "\n\t\t Estado de la Empresa: "+
                    result.getString(12) + "\n\t\t Origen de la Empresa: "+ result.getString (13) + "\n\t\t Número de empleados en la Empresa: "+ result.getString(14);
        }
        return resultQuery;
    }
*/
    public int getNIT_Company() {
        return NIT_Company;
    }

    public String getNumber_Employees() {
        return Number_Employees;
    }

    public String getOrigin_Company() {
        return Origin_Company;
    }

    public String getStatus_Company() {
        return Status_Company;
    }

    public String getWebsite_Contact() {
        return Website_Contact;
    }

    public int getNumber_Contact() {
        return Number_Contact;
    }

    public String getType_Contact() {
        return Type_Contact;
    }

    public String getResidence_Address() {
        return Residence_Address;
    }

    public String getType_Company() {
        return Type_Company;
    }

    public String getResidence_Country() {
        return Residence_Country;
    }

    public String getResidence_State() {
        return Residence_State;
    }

    public String getResidence_Town() {
        return Residence_Town;
    }

    public String getName_Company() {
        return Name_Company;
    }
}
