# DesktopApp

## Aplicación de escritorio para el proyecto integrador

### Contiene:

- Página principal.
- Página de estudiante.
- Página de empresa.
- Página de Usuarios.
- Página de Log.

### Acciones

- Registrar.
- Consultar.
- Modificar.
- Eliminar.

### Compilar

- Linux:

    javac -cp dependences/*:. bin/*java controller/*java view/*java

    java -cp dependences/*:. bin/Main

- Windows:

    javac -cp dependences/*;. bin/*java controller/*java view/*java

    java -cp dependences/*;. bin/Main
